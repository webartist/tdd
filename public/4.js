webpackJsonp([4],{

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(9)
/* script */
var __vue_script__ = __webpack_require__(380)
/* template */
var __vue_template__ = __webpack_require__(381)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\templates\\admin\\Header.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-709aad98", Component.options)
  } else {
    hotAPI.reload("data-v-709aad98", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {};

/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("header", { staticClass: "navbar" }, [
    _c("a", {
      staticClass: "navbar-brand",
      attrs: { href: "/dashboard", "aria-disabled": "false" }
    }),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("ul", { staticClass: "d-md-down-none nav navbar-nav" }, [
      _c(
        "li",
        { staticClass: "nav-item px-3" },
        [
          _c(
            "router-link",
            {
              staticClass: "nav-link",
              attrs: {
                to: { name: "dashboard" },
                target: "_self",
                "aria-disabled": "false"
              }
            },
            [_vm._v("Dashboard")]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler sidebar-toggler d-md-down-none",
        attrs: { type: "button" }
      },
      [_c("i", { staticClass: "fa fa-bars", attrs: { "aria-hidden": "true" } })]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-709aad98", module.exports)
  }
}

/***/ })

});