import VueRouter from 'vue-router'


const Admin = () => import ('./layouts/Admin')
const DashboardView = () => import('./views/admin/Dashboard')
const LoginView = () => import('./auth/Login')


let router = [
        {
            path: '/',
            name:'index',
            redirect:'login',
        },
        {
            path: '/admin',
            name:'admin',
            redirect:'dashboard',
            component: Admin,

            children:[
                {
                    path: '/dashboard',
                    name: 'dashboard',
                    component: DashboardView,
                },
            ]
        },

        {
            path: '/login',
            name:'login',
            component: LoginView,
        },


];


const globalRouter = new VueRouter({
    routes:router,
    mode: 'history',
    linkActiveClass: 'nav-active'
});




export default globalRouter;
