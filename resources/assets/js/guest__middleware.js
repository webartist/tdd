import store from '../../store/index.js'

export default (to, from, next) => {
  if (store.getters['auth/check']) {
    next({ name: 'dashboard' })
  } else {
    next()
  }
}
