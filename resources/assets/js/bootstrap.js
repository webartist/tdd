import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import axios from 'axios'
import lodash from 'lodash'
import $ from 'jquery'
import moment from 'moment'
//import router from './routes.js'
import store from './store/index.js'
import router from './router/index.js'





import BootstrapVue from 'bootstrap-vue'
//import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'

window.$ = window.jQuery = $;

window._ = lodash;
window.Vue = Vue;
window.axios = axios;
window.moment = moment;
window.router = router;
window.store = router;



Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(BootstrapVue);




window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.getElementsByName('csrf-token')[0].getAttribute('content');
//window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + jwtToken.getToken();



window.axios.defaults.baseURL = '/api/v1';


